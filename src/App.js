import React, { Component } from 'react';
import './App.css';
import axios from "axios";

class App extends Component {

    state = {
        country :[],
        oneCountry:[],
    };

    getCountry = (name)=>{
        axios.get('https://restcountries.eu/rest/v2/name/' + name).then(response => {
            if (response.data[0].borders.length > 0) {
                Promise.all(response.data[0].borders.map(border => {
                    return axios.get(`https://restcountries.eu/rest/v2/alpha/${border}`)
                })).then(result => {
                    let borders = [];
                    result.map(country => {
                        borders.push(country.data.name);
                        response.data[0].borders = borders;
                        return this.setState({
                            oneCountry: response.data
                        })
                    })
                })
            } else {
                this.setState({oneCountry: response.data});
            }
        })
    };
    componentDidMount() {
        const BASE_URL = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';
        axios.get(BASE_URL).then(response => {
            return Promise.all(response.data.map(post => {
                return axios.get(BASE_URL).then(response => {
                    return {...post};
                });
            }));
        }).then(country => {
            this.setState({country});
        }).catch(error => {
            console.log(error);
        });
    }

  render() {

    return (
      <div className="App">
          <ol className='countrysList'>
              {this.state.country.map((country, index) => {
                  return <li key={index} onClick={()=>this.getCountry(country.name)}>
                                            {country.name}
                         </li>
              })}
          </ol>
          {this.state.oneCountry !== [] ? this.state.oneCountry.map((country,index) => {
              return <div className='countryWrapper' key={index}>
                        <h1 className='name'>{country.name}<img className='flag' src={country.flag} alt="flag"/></h1>
                        <p>capitai: {country.capital}</p>
                        <p>population: {country.population} человеков</p>
                        <p>area: {country.area} km2</p>
                        <p>region: {country.region}</p>
                        <p>nativeName: {country.nativeName}</p>


                        <div className='border'>
                            <h2>Border with:</h2>
                            <ul>
                                {country.borders.map((border,index) => {
                                    return <li key={index}>
                                                 {border}
                                             </li>
                                })}
                            </ul>
                        </div>
                    </div>
          }) : null}
      </div>
    );
  }
}

export default App;
